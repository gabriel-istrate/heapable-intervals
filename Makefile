#/* Program for simulating the Hammersley process on intevals. 
# @Gabriel Istrate :-)
# 
# Made public for the sole purpose of scientific reproducibility of results 
# from the paper "Heapability of finite partial orders"
# 
# By using this program you explicitly agree
# 
# 1. not to held the author of the program liable for anything related to the program or its functioning.
# 
# 2. to give proper credit to the author of the program, if needed. 
# 
# In return this license gives you a right to use it any way you like whatsoever# :) 
# 
# @gabriel.istrate@e-uvt.ro, 2017
# 
# I grant you license to use this program for any purpose whatsoever
# By using this program, you also agree to assume full responsibility
# for any problem that may arise from its use. In other words, you are free to u# se the program  at your own risk, and you agree not to sue me :).
#


# 
# "This work was supported by a grant of
# Ministery of Research and Innovation, CNCS - UEFISCDI, project number
# PN-III-P4-ID-PCE-2016-0842, within PNCDI III "
# 
#*/


all: interval.cc interval.h
	g++ --std=c++11 interval.cc -o interval -lm

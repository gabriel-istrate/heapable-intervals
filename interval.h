
////////////////////////////////////////////////////////////
/*  Program for simulating the Hammersley process on intevals. 
# @Gabriel Istrate :-)
# 
# Made public for the sole purpose of scientific reproducibility of results 
# from the paper "Heapability of finite partial orders"
# 
# By using this program you explicitly agree
# 
# 1. not to held the author of the program liable for anything related to the program or its functioning.
# 
# 2. to give proper credit to the author of the program, if needed. 
# 
# In return this license gives you a right to use it any way you like whatsoever# :) 
# 
# @gabriel.istrate@e-uvt.ro, 2017
# "This work was supported by a grant of Ministery of Research and Innovation, C# NCS - UEFISCDI, project number PN-III-P4-ID-PCE-2016-0842, within PNCDI III "
*/
//////////////////////////////////////////////////////////



#ifndef _INTERVAL_H_
#define _INTERVAL_H_

#include <cstdbool>
#include <fstream>
#include <iostream> 
using namespace std; 

class Interval{

 public:

  Interval();
  Interval(double, double); 
  ~Interval(){};

  double get_left(){return left;};
  double get_right(){return right;};

  friend bool operator<(const Interval &l, const Interval &r) {
      return ( (l.right < r.right )||( (l.right == r.right )&& (l.left < r.left ))); 
    }; 

friend ostream & operator<<(ostream & il, const Interval & l){
  il << "["<< l.left<<","<<l.right<<"]";
  return il; 
};

 friend bool operator== (const Interval &l, const Interval &r){
   return (l.left==r.left)&&(l.right==r.right); 
 }
  
  
 private:

  double left;
  double right; 

}; 


#endif 

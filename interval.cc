////////////////////////////////////////////////////////////
/*  Program for simulating the Hammersley process on intevals. 
# @Gabriel Istrate :-)
# 
# Made public for the sole purpose of scientific reproducibility of results 
# from the paper "Heapability of finite partial orders"
# 
# By using this program you explicitly agree
# 
# 1. not to held the author of the program liable for anything related to the program or its functioning.
# 
# 2. to give proper credit to the author of the program, if needed. 
# 
# In return this license gives you a right to use it any way you like whatsoever# :) 
# 
# @gabriel.istrate@e-uvt.ro, 2017
# "This work was supported by a grant of Ministery of Research and Innovation, C# NCS - UEFISCDI, project number PN-III-P4-ID-PCE-2016-0842, within PNCDI III "
*/
//////////////////////////////////////////////////////////


#include "interval.h"
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <algorithm>
#include <vector> 
#include <map> 

using namespace std; 

int constant(int, int, int, int);
int debug(int, int, int);



Interval::Interval(){

  left = drand48();
  right = drand48();
  if (left>right){
    double x=left;
    left=right;
    right=x;
  }
}


Interval::Interval(double a, double b)
{
  left=min(a,b);
  right=max(a,b);
}


int main(int argc, char **argv)
{

 int arity, nr_samples, nr_steps;

  //cout << arity<< nr_samples<<nr_steps<< endl; 

  int s; 
  while (true){
     cout << "EXPERIMENT: 1 (constant determination);  0 (debug); anything else: QUIT"<< endl; 

    cin >> s;
    if ((s!=0)&&(s!=1)) exit(0);
    
    cout <<endl<<  "parameters: (arity nr_samples nr_steps): ";
    cin >> arity >> nr_samples >> nr_steps;
     int type;

  cout << "sequences or sets of random intervals: "; 
  cout << "sets: 0, sequences: 1"<< endl; 
  cin >> type; cout << endl<< endl; 
    // cout << "arity: "<< arity << " nr. samples: "<< nr_samples<< "nr. steps: "<< nr_steps<<endl;
   
    switch(s){
    case 0: debug(arity, nr_steps,type); break;
    case 1: constant(arity,nr_samples,nr_steps,type); break;
      // case 2: histogram(arity, nr_steps);break;
      // case 3: percentages(arity,nr_steps);break;
    default: exit(0); 
    }
  }
}
  
int constant(int arity, int nr_samples, int nr_steps, int type)  
{

 

  srand48(getpid());
  Interval next_to_insert;
  double pred;
  int no_heaps, total_no_heaps=0; 

  no_heaps = 0;

  for (int i=0;i<nr_samples;i++){

    total_no_heaps=0; 
    std::vector<Interval> S(nr_steps);
    std::map<double,int> Live;
    std::map<double,int> All; 

    for(int j=0;j<nr_steps;j++)
      S[j]=*(new Interval); 

    /* cout << "initial array: "<< endl;
    for (std::vector<Interval>::iterator a=S.begin();a !=S.end();a++)
      std::cout << *a;
      std::cout << endl<<endl; */
    if (type == 0) 
      std::sort(S.begin(),S.end()); 

    /* cout << "sorted array: "<< endl;
    for (std::vector<Interval>::iterator a=S.begin();a !=S.end();a++)
      std::cout << *a;
      std::cout << endl<<endl;*/
    
    for (std::vector<Interval>::iterator a = S.begin(); a != S.end(); a++) {
      //  std::cout << "inserting "<< a->get_right()<<" ("<< *a << ")"<< endl;
      
      // hack 
      // we temporarily insert the left element among the live ones,
      // together with the right ones.
      
      Live[a->get_left()]=arity;
      Live[a->get_right()]=arity;

       // we insert the right element in the map of all elements.  

      All[a->get_right()]=arity;

      // find the predecessor of a->left among the Live particles

      std::map<double,int>::iterator pos=Live.find(a->get_left());

      // cout << "finding "<< pos->first <<endl;
      double first_el=Live.begin()->first; 
      // cout << "first: "<< alive.begin()->first<<endl; 
      
    

      if (pos->first == first_el){
	no_heaps++;
	//	cout << a->get_left()<< " has no predecessor."<< endl; 
	//	cout << "increasing no. heaps to "<< no_heaps<<endl<<endl;
	Live.erase(a->get_left());
      }else{
	std::map<double,int>::iterator alt = std::prev(pos);
	//	cout << "predecessor of "<<a->get_left()<<": "<<alt->first<<endl; 
	pred=alt->first;
	//	cout << "deleting one life from "<< pred<< endl<<endl;
	
	Live[pred]-=1;
	All[pred]-=1; 
	
	if(Live[pred]==0){
	  //  cout <<pred<< " is dead."<< endl<<endl; 
	  Live.erase(pred); 
	}
	// finally, erase the bogus left element. 
	Live.erase(a->get_left());
      }   
      
      
    }
    total_no_heaps+=no_heaps; 
  } 
  
  double average = (double)total_no_heaps/nr_samples; 
  // cout << "average: "<< average<< endl;
  cout <<"steps: "<< nr_steps<< " average: "<< average<< " constant: "<< average/nr_steps<< endl;

  return 0; 
}



// ////////////////////////////////////////////////////////////////////
/*

Function that prints values as it goes, for debugging purposes
									       
*/
// //////////////////////////////////////////////////////

int debug(int arity, int nr_steps, int type)  
{

  cout << endl<< endl; 

  srand48(getpid());
  Interval next_to_insert;
  double pred;
  int no_heaps, total_no_heaps=0; 

  no_heaps = 0;

 

    total_no_heaps=0; 
    std::vector<Interval> S(nr_steps);
    std::map<double,int> Live;
    std::map<double,int> All; 

    for(int j=0;j<nr_steps;j++)
      S[j]=*(new Interval); 

    /* cout << "initial array: "<< endl;
    for (std::vector<Interval>::iterator a=S.begin();a !=S.end();a++)
      std::cout << *a;
      std::cout << endl<<endl; */
    if (type == 0) 
      std::sort(S.begin(),S.end()); 

    /* cout << "sorted array: "<< endl;
    for (std::vector<Interval>::iterator a=S.begin();a !=S.end();a++)
      std::cout << *a;
      std::cout << endl<<endl;*/
    
    for (std::vector<Interval>::iterator a = S.begin(); a != S.end(); a++) {
       std::cout << "inserting "<< a->get_right()<<" ("<< *a << ")"<< endl;
      
      // hack 
      // we temporarily insert the left element among the live ones,
      // together with the right ones.
      
      Live[a->get_left()]=arity;
      Live[a->get_right()]=arity;

       // we insert the right element in the map of all elements.  

      All[a->get_right()]=arity;

      // find the predecessor of a->left among the Live particles

      std::map<double,int>::iterator pos=Live.find(a->get_left());

      // cout << "finding "<< pos->first <<endl;
      double first_el=Live.begin()->first; 
      // cout << "first: "<< alive.begin()->first<<endl; 
      
    

      if (pos->first == first_el){
	no_heaps++;
	cout << a->get_left()<< " has no predecessor."<< endl; 
	cout << "increasing no. heaps to "<< no_heaps<<endl<<endl;
	Live.erase(a->get_left());
	 
      }else{
	std::map<double,int>::iterator alt = std::prev(pos);
		cout << "predecessor of "<<a->get_left()<<": "<<alt->first<<endl; 
	pred=alt->first;
		cout << "deleting one life from "<< pred<< endl<<endl;
	
	Live[pred]-=1;
	All[pred]-=1; 
	
	if(Live[pred]==0){
	    cout <<pred<< " is dead."<< endl<<endl; 
	  Live.erase(pred); 
	}
	// finally, erase the bogus left element. 
	Live.erase(a->get_left());
      }  

      cout << "Live particles: "<< endl;
      for(auto it = Live.cbegin(); it != Live.cend(); ++it)
{
    std::cout << it->first << " " << it->second << " live(s) \n";
}
      cout << endl<<endl; 
    }


  
 
  // cout << "average: "<< average<< endl;
  cout <<"steps: "<< nr_steps<< " heaps: "<< no_heaps<< " constant: "<< no_heaps/nr_steps<< endl;

  return 0; 
}
